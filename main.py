from open_corporates_client import OpenCorporatesClient
import click
client = OpenCorporatesClient("vDotJkmsaQTXjzHZsj2a")

@click.command()
@click.option('--name', help='Name of the company')
def get_company_info(name):
    company_info = name.split(";")
    search_token = "{0}".format(company_info[0])
    try:
        company = client.get_company(search_token)
        print(company.to_csv())
    except Exception as e:
        print("{0} not found: {1}".format(name,e))

if __name__ == '__main__':
    get_company_info()