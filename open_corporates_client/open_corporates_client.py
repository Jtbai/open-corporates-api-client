import re
import requests as req
from scrapy.crawler import CrawlerProcess, Crawler
from scrapy.utils.project import get_project_settings
from open_corporates_client.crawler.spiders.company import CompanySpider
import unicodedata
from utils import Company, Client
from os.path import join
import json

class OpenCorporatesClient(Client):

    API_END_POINT = "https://api.opencorporates.com/"
    SEARCH_END_POINT = "https://api.opencorporates.com/companies/search?q="
    COMPANY_END_POINT = "https://api.opencorporates.com/companies/"
    api_key = None
    country_code = 'ca'
    crawling_process = None


    LISTING_ID_KEY = 'company_number'
    NAME_KEY = 'name'
    RESULTS_KEY = 'results'
    COMPANIES_KEY = 'companies'
    COMPANY_KEY_NUMBER = 'company_number'
    COMPANY_KEY = 'company'
    ADDRESS_KEY = 'registered_address_in_full'
    JURISDICTION_KEY = 'jurisdiction_code'
    ACTIVITY_CODE_KEY = 'activity_code'
    ALTERNATIVE_NAME_KEY = 'alternative_name'


    def __init__(self,api_key=None):
        # self.crawling_process = CrawlerProcess(get_project_settings())
        if api_key is not None:
            self.api_key = api_key

    def _get_companies_from_api(self,name):
        # Le order=score permet d'avoir les recherches en ordre de relevance selon elasticsearch
        # sinon les resultats sont classes par ordre alphabetiques
        name = ''.join((c for c in unicodedata.normalize('NFD', name) if unicodedata.category(c) != 'Mn'))
        get_uri = self.SEARCH_END_POINT + name + "&order=score&jurisdiction_code=ca_qc"
        if self.api_key is not None:
            get_uri += "&api_token={0}".format(self.api_key)
        return req.get(get_uri)
    #

    def _get_companies(self, name):
        response = self._get_companies_from_api(name)
        response = self._decode_and_load_json_response (response.content)
        return self.extract_companies_from_json_response(response)

    def get_companies(self,name):
        companies_response_content = self._get_companies(name)
        companies = []
        if len(companies_response_content)>0:
            for company_information in companies_response_content:
                company_number = company_information[self.COMPANY_KEY][self.COMPANY_KEY_NUMBER]
                company_jurisdiction = company_information[self.COMPANY_KEY][self.JURISDICTION_KEY]
                company_api_information = self._get_company_information_from_api(company_jurisdiction, company_number)
                current_company = self._build_company_from_api_input(company_api_information)
                companies.append(current_company)


        return companies


    def _get_company_information_from_api(self, jurisdiction, company_number):
        # Le order=score permet d'avoir les recherches en ordre de relevance selon elasticsearch
        # sinon les resultats sont classes par ordre alphabetiques
        get_uri = self.COMPANY_END_POINT + jurisdiction +'/' + company_number
        if self.api_key is not None:
            get_uri += "?api_token={0}".format(self.api_key)
        response = req.get(get_uri)
        decoded_response = self._decode_and_load_json_response(response.content)

        return decoded_response


    @staticmethod
    def extract_companies_from_json_response(prepared_response):
        if OpenCorporatesClient.RESULTS_KEY in prepared_response:
            return OpenCorporatesClient.extract_companies_from_json_response(prepared_response[OpenCorporatesClient.RESULTS_KEY])
        if OpenCorporatesClient.COMPANIES_KEY in prepared_response:
            return OpenCorporatesClient.extract_companies_from_json_response(prepared_response[OpenCorporatesClient.COMPANIES_KEY])
        else:
            return prepared_response

    @staticmethod
    def extract_company_info_from_company_item(company_item):
        company_information = {}
        company_information['activity_code'] = company_item.activity_code
        company_information['alternative_name'] = company_item.alternative_name
        return company_information


    @staticmethod
    def prepare_name(unformated_name):
        template = "([-\[\]\/\{\}\(\)\\!\'\+\?\.\\\^\$\|\&\,\"])"
        return re.sub(template,"",unformated_name)

    @staticmethod
    def _build_company_from_api_input(company_from_api):
        company = Company()
        company_info = company_from_api['results']['company']
        company.registered_name = company_info[OpenCorporatesClient.NAME_KEY]
        company.address = company_info[OpenCorporatesClient.ADDRESS_KEY]
        if len(company_info['alternative_names']) > 0:
            company.other[OpenCorporatesClient.ALTERNATIVE_NAME_KEY] = company_info['alternative_names'][-1]['company_name']

        if len(company_info['industry_codes']) > 0:
            company.activity_code = company_info['industry_codes'][0]['industry_code']['code']



        if OpenCorporatesClient.ALTERNATIVE_NAME_KEY in company.other and ( OpenCorporatesClient._is_company_a_numero(company.registered_name) or OpenCorporatesClient._both_names_are_very_different(company.registered_name,company.other[OpenCorporatesClient.ALTERNATIVE_NAME_KEY])):

            company.name = company.other[OpenCorporatesClient.ALTERNATIVE_NAME_KEY]
        else:
            company.name = company.registered_name

        return company

    @staticmethod
    def _is_company_a_numero(company_name):
        qc_company_numero_patern = '[0-9]{4}-[0-9]{4} Q'
        if re.search(qc_company_numero_patern,company_name,re.IGNORECASE):
            return True
        else:
            return False

    @staticmethod
    def _both_names_are_very_different(name1, name2):
        from nltk.metrics import edit_distance
        return edit_distance(name1,name2) > len(name1)/2





    @staticmethod
    def _update_company_with_crawler_information(company_detail_from_crawler,company):
        company.other[OpenCorporatesClient.ALTERNATIVE_NAME_KEY] = company_detail_from_crawler[OpenCorporatesClient.ALTERNATIVE_NAME_KEY]
        company.activity_code = company_detail_from_crawler[OpenCorporatesClient.ACTIVITY_CODE_KEY]
