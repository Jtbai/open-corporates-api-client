# -*- coding: utf-8 -*-
from open_corporates_client.crawler.items import Company
import scrapy
import re
from os.path import join

class CompanySpider(scrapy.Spider):
    name = 'company'
    company = None

    def __init__(self, *args,**kwargs):
        self.company = Company()
        super(CompanySpider,self).__init__(*args,**kwargs)

    def start_requests(self):

        url = join('https://opencorporates.com/companies/ca_qc/',self.company_number)

        yield scrapy.Request(url,self.parse)

    def parse(self, response):
        self.company.activity_code = self._obtain_activity_code(response)
        self.company.alternative_name = self._obtain_alternative_name(response)
        # yield self.company


    def _obtain_activity_code(self,response):
        activity_code = response.css('dd.industry_codes li::text').extract_first()
        activity_code_template = r'([0-9]+)'
        re_capture_group = re.match(activity_code_template,activity_code)
        if re_capture_group is not None :
            return re_capture_group.group()
        else:
            return None

    def _obtain_alternative_name(self,response):
        alternative_names = response.css('dd.alternative_names li::text').extract_first()

        if alternative_names is not [] :
            return alternative_names
        else:
            return None

