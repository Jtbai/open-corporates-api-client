class Company:

    name = None
    company_number = None
    address = None
    alternative_name = None
    activity_code = None


    def __init__(self,company_information):
        self.set_parameters(company_information)

    def set_parameters(self, information_dict):
        for attribute in information_dict.items():
            setattr(self,attribute[0],attribute[1])
