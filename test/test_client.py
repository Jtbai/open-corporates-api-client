import unittest
from open_corporates_client.open_corporates_client import OpenCorporatesClient
class TestClient(unittest.TestCase):

    def test_get_company_from_api_without_key(self):
        client = OpenCorporatesClient()
        companies = client._get_companies_from_api('bombardier')
        self.assertEqual(companies.status_code,200)

    def test_get_company(self):
        client = OpenCorporatesClient()
        companies = client._get_companies('bombardier')
        # self.assertTrue(open_corporates_client._get_companies_from_api.called)
        # il faut que je trouve comment tester ma methode...
