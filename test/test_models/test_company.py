import unittest
import open_corporates_client.models as models

class TestCompany(unittest.TestCase):

    def test_constructeur(self):
        info = {
            "name":"TheCo",
            "address":"2020 foostreet",
            "company_number":"1234",
            "sector":"4321",
        }

        company = models.Company(info)

        self.assertEqual("TheCo",company.name)
        self.assertEqual("2020 foostreet",company.address)
        self.assertEqual("1234",company.company_number)
        self.assertEqual("4321",company.sector)