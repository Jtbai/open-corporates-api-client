import json
import unittest
from open_corporates_client.open_corporates_client import OpenCorporatesClient


class ClientStaticMethodTest(unittest.TestCase):

    def test_prepare_name(self):
        unprepared_name = "\"''!...,&&"
        prepared_name = OpenCorporatesClient.prepare_name(unprepared_name)
        self.assertEquals('',prepared_name)

